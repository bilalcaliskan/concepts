## Infrastructure as Code
> What is it?

**Infrastructure as Code** is the process of managing the underlying infrastructure just with code. Version control systems 
are very important for Infrastructure as Code since it provides the way of keeping history of changes and collaborative work 
environment between different team members. Popular version control based software development platforms like Github also 
provides ability to automate the whole process and minimize the human error.

There are 2 different approach for Infrastructure as Code, declarative or imperative approach.

With **declarative approach**, you define the desired state of the system, including what resources you need and any 
properties they should have, and an IaC tool will configure it for you. If you make changes to the desired 
state, a declarative IaC tool will apply those changes for you. **Terraform** and **Puppet** can be considered as 
declarative IaC tool. Long story short; you say what to do and the tool will handle the rest.

With **imperative approach**, you define specific commands needed to achieve your desired configuration. Long story 
short; you say what to do and how to do it. **Chef** is known as imperative IaC tool.

> Why would I want it?

Infrastructure as Code minimizes the manual processes while making changes on the infrastructure and keeps the history of 
these changes and it provides the ability to rollback changes fastly when undesired situations occurred. Also with that kind of 
approach, you can create more collaborative environment where you enforce your Infrastructure developers to follow some coding 
standarts, pull request based development flow etc.

> Are there any alternatives?

As far as i know, there is no alternative that provisions resources as effectively as IaC.

## Observability
> Please explain this term in the context of microservices.

Observability is the term of measuring the internal states of a microservice from different aspects. For a proper "observable" microservice, application should provide some outputs.

> What do we want to observe?

There are different kind of observability patterns like log aggregation, application metrics, distributed tracing etc. To be able to know what is happening on our system, we should be able to 
gather those outputs from applications. 

> What kind of challenges do you see in a distributed environment?

I think, the first and the major problem is addressing the problem when undesired things happened on our system. Assume that you have 10 microservices and there is a client-faced 
problem. If you don't apply proper observability patterns for your needs, it is nearly impossible to address the problem. All patterns are important but the most important pattern i think is 
distributed tracing. In a distributed environment if you don't provide that, i guarantee that you will be blind about your own application environment. 

> How can we solve them?

As i mentioned on previously, we can apply below patterns:
- log aggregation - if we provide that pattern then it will be very easy to address the problem at application level.
- application metrics - if we provide that pattern then we can address the problems at application level. This pattern also helps us to address performance related problems.
- distributed tracing - with that pattern, we can address the real problem on a distributed environment. 
- etc...

## Security
> Imagine you would join our team and put in charge of securing our AWS infrastructure. What are the first three things that you check, to limit the risk of a breach? Explain why.

First thing first I would use [Trusted Advisor](https://aws.amazon.com/premiumsupport/technology/trusted-advisor/) service if i could. And then would check the below items:
- I would check if MFA is enabled on present IAM users including root user
- I would check if there is a password policy for IAM users
- I would check user and group management on IAM
- I would check security groups and try to identify if these rules follows the best practices
- I would check if IAM user management follows the least privilege principal

Above checklisted items would give me an insight about the security level on AWS. Securing root account and following least privileged principal is the headline for your question i think. 
